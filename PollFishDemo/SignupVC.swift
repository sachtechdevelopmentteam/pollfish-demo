//
//  SignupVC.swift
//  PollFishDemo
//
//  Created by Kapil Dhawan on 26/03/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseCore
import FirebaseDatabase

class SignupVC: UIViewController {

    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
   
    @IBOutlet weak var tfCity: UITextField!
    
    @IBOutlet weak var tfZip: UITextField!
    @IBOutlet weak var tfState: UITextField!
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database().reference().child("users")
    }
    

    @IBAction func btnRegister(_ sender: Any) {
        if (self.tfFirstName.text!.isEmpty || self.tfLastName.text!.isEmpty || self.tfPassword.text!.isEmpty || self.tfEmail.text!.isEmpty || self.tfCity.text!.isEmpty || self.tfState.text!.isEmpty || self.tfZip.text!.isEmpty){
            
            let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter data required", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
            
        else if (tfPassword.text?.count)! < 6 {
            let alertController = UIAlertController(title: "Invalid Password", message: "Please enter a password of atleast 8 characters", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else if !isValidEmail(self.tfEmail.text!) {
            let alertController = UIAlertController(title: "Invalid Format", message: "Please enter valid Email address", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            Auth.auth().createUser(withEmail: self.tfEmail.text!, password: self.tfPassword.text!) { (user, error) in
                if error == nil{
                    let key = self.ref.childByAutoId().key
                    let user = [
                        "First_Name" : self.tfFirstName.text!,
                        "Last_Name" : self.tfLastName.text!,
                        "Email" : self.tfEmail.text!,
                        "City" : self.tfCity.text!,
                        "State" : self.tfState.text!,
                        "Zip_Code" : self.tfZip.text!,
                        "Completed_Survey" : 0
                        ] as [String : Any]
                    self.ref.child(key!).setValue(user)
                    ToastView.shared.short(self.view, txt_msg: "User Registered Successfully.")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let data = storyboard.instantiateViewController(withIdentifier: "TakeSurveyVC") as! TakeSurveyVC
                    data.useremail = self.tfEmail.text!
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        self.navigationController?.pushViewController(data, animated: true)
                    })
                    
                }
                
                else{
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }

        
           }
    }
    
    

    @IBAction func btnSignIn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func isValidEmail(_ testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

//
//  TakeSurveyVC.swift
//  PollFishDemo
//
//  Created by Kapil Dhawan on 27/03/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseCore

struct userModel {
    var completedSurvey:Int!
    var state: String!
    var zip: String!
    var email: String!
    var city: String!
    var firstname: String!
    var lastname: String!
    
    init(dict: NSDictionary) {
        completedSurvey = dict["Completed_Survey"] as? Int
        city = dict["City"] as? String
        state = dict["State"] as? String
        zip = dict["Zip_Code"] as? String
        email = dict["Email"] as? String
        firstname = dict["First_Name"] as? String
        lastname = dict["Last_Name"] as? String
    }
}

class TakeSurveyVC: UIViewController {
    
    
    @IBOutlet weak var lblSurveyCompleted: UILabel!
    
    var useremail = String()
    var ref: DatabaseReference!
    var userkey = String()
    var userData = userModel(dict: [:])
    var completedSurvey = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let idtosearch = useremail
        self.ref = Database.database().reference().child("users")
        
        Pollfish.show()
        Pollfish.initAtPosition(Int32(PollfishPosition.PollFishPositionMiddleRight.rawValue),
                                withPadding: 0,
                                andDeveloperKey: "0c7890be-3752-496b-927a-0dca31f0c983" ,
                                andDebuggable: false,
                                andCustomMode: false)
        
        NotificationCenter.default.addObserver(self, selector:#selector(pollfishCompleted), name:
            NSNotification.Name(rawValue: "PollfishSurveyCompleted"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(pollfishReceived), name:
            NSNotification.Name(rawValue: "PollfishSurveyReceived"), object: nil)
        
        
        self.ref.queryOrdered(byChild: "Email").queryEqual(toValue: idtosearch).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists(){
                
                for child in snapshot.children {
                    self.userkey = (child as AnyObject).key as String
                }
                
                let data = snapshot.value as! NSDictionary
                let value = data.allValues
                let dat = value as! [NSDictionary]
                self.userData = userModel(dict: dat[0])
                DispatchQueue.main.async {
                    self.completedSurvey = self.userData.completedSurvey!
                    self.lblSurveyCompleted.text = String(self.completedSurvey)
                }
                
            }
            
        })
       
    }
     @objc func pollfishReceived()
    {
        ToastView.shared.short(self.view, txt_msg: "Pollfish Survey received!")
        Pollfish.show()
    }
    @objc func pollfishCompleted()
    {
        let total = self.completedSurvey + 1
        self.lblSurveyCompleted.text = String(total)
        let user = [
            "First_Name" : self.userData.firstname!,
            "Last_Name" : self.userData.lastname!,
            "Email" : self.userData.email!,
            "City" : self.userData.city,
            "State" : self.userData.state,
            "Zip_Code" : self.userData.zip,
            "Completed_Survey" : total
            ] as [String : Any]
        self.ref.child(self.userkey).updateChildValues(user)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            ToastView.shared.short(self.view, txt_msg: "Pollfish Survey Completed!")
        })
        
    }

    @IBAction func btnShow(_ sender: Any) {
        Pollfish.show()
    }
    @IBAction func btnHide(_ sender: Any) {
        Pollfish.hide()
    }
}

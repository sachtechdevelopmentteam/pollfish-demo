//
//  LoginVC.swift
//  PollFishDemo
//
//  Created by Kapil Dhawan on 26/03/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginVC: UIViewController {

    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationController?.navigationBar.isHidden = true
        
    }
    

    @IBAction func btnLogin(_ sender: Any) {
        if (self.tfPassword.text!.isEmpty || self.tfEmail.text!.isEmpty) {
            let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter data required", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
    
        else {
            Auth.auth().signIn(withEmail: tfEmail.text!, password: tfPassword.text!) { (user, error) in
                if error == nil{
                    ToastView.shared.short(self.view, txt_msg: "User Logged In Successfully.")
                   
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let data = storyboard.instantiateViewController(withIdentifier: "TakeSurveyVC") as! TakeSurveyVC
                    data.useremail = self.tfEmail.text!
                      DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                         self.navigationController?.pushViewController(data, animated: true)
                    })
                
                }
                else{
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                    

                }
            }
        }
    }
    
    @IBAction func btnSignup(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        
        self.navigationController?.pushViewController(data, animated: true)
    }
}

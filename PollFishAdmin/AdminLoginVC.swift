

import UIKit

class AdminLoginVC: UIViewController {

    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    self.navigationController?.navigationBar.isHidden = true
        
    }
    
@IBAction func btnLogin(_ sender: Any) {
    if (self.tfPassword.text!.isEmpty || self.tfEmail.text!.isEmpty) {
        let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter data required", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    else if (self.tfPassword.text! == "1234" && self.tfEmail.text! == "admin@gmail.com" ) {
    ToastView.shared.short(self.view, txt_msg: "User Logged In Successfully.")
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let data = storyboard.instantiateViewController(withIdentifier: "SurveyVC") as! SurveyVC
    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
        self.navigationController?.pushViewController(data, animated: true)
    })
    }
    else {
        let alertController = UIAlertController(title: "Invalid Login", message: "Enter the valid credentials.", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
}
}
}

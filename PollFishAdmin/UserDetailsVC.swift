//
//  UserDetailsVC.swift
//  PollFishAdmin
//
//  Created by Kapil Dhawan on 28/03/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit

class UserDetailsVC: UIViewController {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblZipCode: UILabel!
    var name = String()
    var email = String()
    var city = String()
    var state = String()
    var zip = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblName.text = name
        lblEmail.text = email
        lblCity.text = city
        lblState.text = state
        lblZipCode.text = zip
        
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

//
//  SurveyCell.swift
//  PollFishAdmin
//
//  Created by Kapil Dhawan on 27/03/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit

class SurveyCell: UITableViewCell {

    
    @IBOutlet weak var lblSurveyCompleted: UILabel!
    @IBOutlet weak var lblUsers: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  SurveyVC.swift
//  PollFishAdmin
//
//  Created by Kapil Dhawan on 27/03/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseDatabase
import SVProgressHUD

struct Users{
    var completedSurvey:Int!
    var city: String!
    var state: String!
    var zip: String!
    var email: String!
    var firstname: String!
    var lastname: String!
    
    init(dict: [String: AnyObject]) {
        completedSurvey = dict["Completed_Survey"] as? Int
        city = dict["City"] as? String
        state = dict["State"] as? String
        zip = dict["Zip_Code"] as? String
        email = dict["Email"] as? String
        firstname = dict["First_Name"] as? String
        lastname = dict["Last_Name"] as? String
    }
}

class SurveyVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    

    @IBOutlet weak var tblSurvey: UITableView!
    
    var Userdata = [Users]()
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblSurvey.delegate = self
        self.tblSurvey.dataSource = self
        retrieveData()
    }
    func retrieveData() {
        SVProgressHUD.show()
        self.ref = Database.database().reference().child("users")
        self.ref.observe(DataEventType.value, with: {(snapshot) in
            if snapshot.childrenCount > 0 {
               self.Userdata.removeAll()
                
                for user in snapshot.children.allObjects as! [DataSnapshot] {
                    let userObject = user.value as? [String: AnyObject]
                    self.Userdata.append(Users(dict: userObject!))
                   
                }
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.tblSurvey.reloadData()
                }
                
            } else {
                ToastView.shared.short(self.view, txt_msg: "No registered users.")
            }
            
        })
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Userdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SurveyCell", for: indexPath) as! SurveyCell
        let firstname = Userdata[indexPath.row].firstname!
        let lastname = Userdata[indexPath.row].lastname!
        let name = "\(firstname) \(lastname)"
        cell.lblUsers.text = name
        cell.lblSurveyCompleted.text = String(Userdata[indexPath.row].completedSurvey)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
        let firstname = Userdata[indexPath.row].firstname!
        let lastname = Userdata[indexPath.row].lastname!
        let name = "\(firstname) \(lastname)"
        data.name = name
        data.email = Userdata[indexPath.row].email!
        data.city = Userdata[indexPath.row].city!
        data.state = Userdata[indexPath.row].state!
        data.zip = Userdata[indexPath.row].zip!
        
        
        
        self.navigationController?.pushViewController(data, animated: true)
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    

}
